<?php
namespace App\Controller ;

use App\Entity\Project;
use App\Service\ProjectService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use App\Service\ProjectService as ServiceProject;
;


class ProjectController extends AbstractController {

    /**
     * @Route("/project", name="project_list")
     * @Method ({"GET"})
     */
    public function article(){

        $project  = new ServiceProject($this->getDoctrine()->getManager(),Project::class);
        $projects = $project->getAllproject();
        return $this->render('projects/index.html.twig', array
        ('projects' => $projects));
    }

    /**
     * @Route ("/project/new", name="new_project")
     * Method ({"GET", "POST"})
     */
    public function new(Request $request){
        $project = new Project();

        $form = $this->createFormBuilder($project)
            ->add('name', TextType::class, array('attr' =>array('class' => 'form-control')))
            ->add('description', TextareaType::class, array('required' =>false,
                'attr' =>array('class' =>'form-control')))
                ->add('created_at', TextType::class, array(
                    'required' => false,
                    'empty_data' => null,
                    'attr' => array(
                        'placeholder' => 'mm/dd/yyyy'
                    )))
            ->add('deadline', TextType::class, array(
                'required' => false,
                'empty_data' => null,
                'attr' => array(
                    'placeholder' => 'mm/dd/yyyy'
                )))

            ->add('save', SubmitType::class, array(
                'label' =>'Create',
                'attr' =>array('class'=>'btn btn-primary mt-3')
            ))
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $project = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($project);
            $entityManager->flush();

            return $this->redirectToRoute('project_list');
        }

        return $this->render('projects/new.html.twig',array(
            'form'=>$form->createView()
        ));
    }

    /**
     * @Route ("/project/{id}", name= "article_show")
     * @Method ({"GET"})
     */

    public function show($id){
        $project  = new ServiceProject($this->getDoctrine()->getManager(),Project::class);
        $project = $project->getProjectId($id);
        return $this->render('projects/show.html.twig', array('project' =>$project));

    }

    /**
     * @Route ("/project/update/{id}", name="update_article")
     * Method ({"GET", "POST"})
     */

    public function update(Request $request, $id){
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Project::class)->find($id);


        $form = $this->createFormBuilder($article)
            ->add('name', TextType::class, array('attr' =>array('class' => 'form-control')))
            ->add('description', TextareaType::class, array('required' =>false,
                'attr' =>array('class' =>'form-control')))
            ->add('save', SubmitType::class, array(
                'label' =>'Update',
                'attr' =>array('class'=>'btn btn-primary mt-3')
            ))
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $article  = new ServiceProject($this->getDoctrine()->getManager(),Project::class);
            $em->flush();

            return $this->redirectToRoute('project_list');
        }

        return $this->render('projects/update.html.twig',array(
            'form'=>$form->createView()
        ));
    }

    /**
     * @Route ("/project/delete/{id}", name="delete")
          * Method ({"GET", "POST"})

     */

    public function delete($id){

        $project  = new ServiceProject($this->getDoctrine()->getManager(),Project::class);
        $project->deleteProject($id);

        return $this->redirectToRoute('project_list');
    }
}


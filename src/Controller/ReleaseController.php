<?php

namespace App\Controller;


use App\Entity\Project;
use App\Entity\Releases;
use App\Service\ClaimService;
use App\Service\ProjectService;
use App\Service\ProjectService as ServiceProject;
use App\Service\RelaseService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ReleaseController extends AbstractController
{
    /**
     * @Route("/project/{id}/release", name="releases")
     */
    public function index($id)
    {

        $relase  = new RelaseService($this->getDoctrine()->getManager(),Releases::class);
        $relases = $relase->getAll();
        return $this->render('release/index.html.twig', array
        ('claims' => $relases,"id"=>$id));
    }

    /**
     * @Route("/project/{id}/release/new", name="release")
     */
    public function new(Request $request,$id){
        $release = new Releases();
        $project = new ProjectService($this->getDoctrine()->getManager(),Project::class);
$proj=$project->getProjectId($id);
        $release->setProject($proj);
        $form = $this->createFormBuilder($release)
            ->add('version', TextType::class, array('attr' =>array('class' => 'form-control')))
            ->add('status', TextType::class, array('required' =>false,
                'attr' =>array('class' =>'form-control')))
            ->add('progress', TextType::class, array('required' =>false,
                'attr' =>array('class' =>'form-control')))
            ->add('description', TextareaType::class, array('required' =>false,
                'attr' =>array('class' =>'form-control')))

            ->add('start_date', TextType::class, array(
                'required' => false,
                'empty_data' => null,
                'attr' => array(
                    'placeholder' => 'mm/dd/yyyy'
                )))
            ->add('release_date', TextType::class, array(
                'required' => false,
                'empty_data' => null,
                'attr' => array(
                    'placeholder' => 'mm/dd/yyyy'
                )))

            ->add('save', SubmitType::class, array(
                'label' =>'Create',
                'attr' =>array('class'=>'btn btn-primary mt-3')
            ))
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $project = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($project);
            $entityManager->flush();

            return $this->redirectToRoute('article_list');
        }

        return $this->render('projects/new.html.twig',array(
            'form'=>$form->createView()
        ));
    }

    /**
     * @Route ("/project/release/update/{id}", name="update_releases")
     * Method ({"GET", "POST"})
     */

    public function update(Request $request, $id){
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Releases::class)->find($id);


        $form = $this->createFormBuilder($article)
            ->add('version', TextType::class, array('attr' =>array('class' => 'form-control')))
            ->add('status', TextType::class, array('required' =>false,
                'attr' =>array('class' =>'form-control')))
            ->add('progress', TextType::class, array('required' =>false,
                'attr' =>array('class' =>'form-control')))
            ->add('description', TextareaType::class, array('required' =>false,
                'attr' =>array('class' =>'form-control')))

            ->add('start_date', TextType::class, array(
                'required' => false,
                'empty_data' => null,
                'attr' => array(
                    'placeholder' => 'mm/dd/yyyy'
                )))
            ->add('release_date', TextType::class, array(
                'required' => false,
                'empty_data' => null,
                'attr' => array(
                    'placeholder' => 'mm/dd/yyyy'
                )))

            ->add('save', SubmitType::class, array(
                'label' =>'Create',
                'attr' =>array('class'=>'btn btn-primary mt-3')
            ))
            ->getForm();


        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $article  = new ServiceProject($this->getDoctrine()->getManager(),Releases::class);
            $em->flush();

            return $this->redirectToRoute('article_list');
        }

        return $this->render('projects/update.html.twig',array(
            'form'=>$form->createView()
        ));
    }
    /**
     * @Route ("/project/release/{id}", name="show_releases")
     * Method ({"GET", "POST"})
     */

    public function show($id){
        $relase  = new RelaseService($this->getDoctrine()->getManager(),Releases::class);
        $relase = $relase->getById($id);
        return $this->render('release/show.html.twig', array('release' =>$relase));

    }

}

<?php

namespace App\Controller;

use App\Entity\Project;
use App\Entity\Releases;
use App\Entity\Report;
use App\Service\ProjectService;
use App\Service\RelaseService;
use App\Service\ReportService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ReportController extends AbstractController
{
    /**
     * @Route("/project/{ids}/release/{id}/report/new", name="report")
     */
        public function new(Request $request,$id){
        $report = new Report();
        $project = new RelaseService($this->getDoctrine()->getManager(),Releases::class);
        $release=$project->getById($id);
        $report->setReleases($release);
        $form = $this->createFormBuilder($report)
            ->add('chart', TextType::class, array('attr' =>array('class' => 'form-control')))

            ->add('save', SubmitType::class, array(
                'label' =>'Create',
                'attr' =>array('class'=>'btn btn-primary mt-3')
            ))->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $project = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($project);
            $entityManager->flush();

            return $this->redirectToRoute('article_list');
        }

            return $this->render('report/new.html.twig',array(
                'form'=>$form->createView()
            ));

    }
    /**
     * @Route("/project/{ids}/release/{id}/report/{idc}", name="claims")
     */
    public function showone($idc)
    {
        $relase = new ReportService($this->getDoctrine()->getManager(), Report::class);
        $relases = $relase->getById($idc);
        return $this->render('report/show.html.twig', array
        ('project' => $relases));
    }
    /**
     * @Route("/project/{ids}/release/{id}/report", name="claim")
     */
    public function show($ids,$id)
    {
        $relase = new ReportService($this->getDoctrine()->getManager(), Report::class);
        $relases = $relase->getReportById($id);
        return $this->render('report/index.html.twig', array
        ('claims' => $relases,"ids"=>$ids,"id"=>$id));
    }
    /**
     * @Route("/project/{ids}/release/{id}/report/delete/{idc}", name="del")
     * Method ({"GET", "POST"})

     */

    public function delete($idc){

        $project  = new ReportService($this->getDoctrine()->getManager(),Report::class);
        $project->deletereport($idc);

        return $this->redirectToRoute('article_list');
    }




}

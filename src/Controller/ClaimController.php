<?php
namespace App\Controller ;

use App\Entity\Claim;
use App\Entity\Project;
use App\Service\ProjectService;
use App\Service\ProjectService as ServiceProject;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use App\Service\ClaimService as ServiceClaim;



class ClaimController extends AbstractController {

    /**
     * @Route("/project/{idp}/release/{idr}/claim", name="claim_list")
     * @Method ({"GET"})
     */
    public function article(){

        $project  = new ServiceProject($this->getDoctrine()->getManager(),Claim::class);
        $projects = $project->getAllproject();
        return $this->render('claims/index.html.twig', array
        ('claims' => $projects));
    }

    /**
     * @Route ("/article/new", name="new_article")
     * Method ({"GET", "POST"})
     */
    public function new(Request $request){
        $claim = new Claim();

        $form = $this->createFormBuilder($claim)
            ->add('name', TextType::class, array('attr' =>array('class' => 'form-control')))
            ->add('description', TextareaType::class, array('required' =>false,
                'attr' =>array('class' =>'form-control')))
            ->add('save', SubmitType::class, array(
                'label' =>'Create',
                'attr' =>array('class'=>'btn btn-primary mt-3')
            ))
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $article = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($article);
            $entityManager->flush();

            return $this->redirectToRoute('article_list');
        }

        return $this->render('projects/new.html.twig',array(
            'form'=>$form->createView()
        ));
    }

    /**
     * @Route ("/project/claim/{id}", name= "claim_show")
     * @Method ({"GET"})
     */

    public function show($id){
        $project  = new ServiceProject($this->getDoctrine()->getManager(),Claim::class);
        $project = $project->getProjectId($id);
        return $this->render('claims/show.html.twig', array('project' =>$project));

    }

    /**
     * @Route ("/project/update/{id}", name="update_article")
     * Method ({"GET", "POST"})
     */

    public function update(Request $request, $id){

        $article = $this->getDoctrine()->getRepository(Claim::class)->find($id);

        $form = $this->createFormBuilder($article)
            ->add('name', TextType::class, array('attr' =>array('class' => 'form-control')))
            ->add('description', TextareaType::class, array('required' =>false,
                'attr' =>array('class' =>'form-control')))
            ->add('save', SubmitType::class, array(
                'label' =>'Update',
                'attr' =>array('class'=>'btn btn-primary mt-3')
            ))
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $article  = new ServiceProject($this->getDoctrine()->getManager(),Claim::class);
            $article->addproject();

            return $this->redirectToRoute('article_list');
        }

        return $this->render('projects/update.html.twig',array(
            'form'=>$form->createView()
        ));
    }

    /**
     * @Route ("/project/delete/{id}")
     * @Method ({"DELETE"})
     */

    public function delete(Request $request, $id){

        $project  = new ServiceProject($this->getDoctrine()->getManager(),Claim::class);
        $project->deleteProject($id);

        return $this->redirectToRoute('article_list');
    }
}


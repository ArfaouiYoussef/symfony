<?php

namespace App\Controller;

use App\Entity\Claim;
use App\Entity\Releases;
use App\Entity\Report;
use App\Service\ClaimService;
use App\Service\RelaseService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ClaimsController extends AbstractController
{
    /**
     * @Route("/project/{ids}/release/{id}/claim/new", name="claim")
     */
    public function new(Request $request, $id)
    {
        $report = new Claim();
        $project = new RelaseService($this->getDoctrine()->getManager(), Releases::class);
        $release = $project->getById($id);
        $report->setReleases($release);
        $form = $this->createFormBuilder($report)
            ->add('content', TextType::class, array('attr' => array('class' => 'form-control')))
            ->add('save', SubmitType::class, array(
                'label' => 'Create',
                'attr' => array('class' => 'btn btn-primary mt-3')
            ))->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $project = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($project);
            $entityManager->flush();

            return $this->redirectToRoute('article_list');
        }

        return $this->render('claims/new.html.twig', array(
            'form' => $form->createView()
        ));

    }

    /**
     * @Route("/project/{ids}/release/{id}/claim", name="claim")
     */
    public function show($ids,$id)
    {
        $relase = new ClaimService($this->getDoctrine()->getManager(), Claim::class);
        $relases = $relase->getClaimByid($id);
        return $this->render('claims/index.html.twig', array
        ('claims' => $relases,"ids"=>$ids,"id"=>$id));
    }
}

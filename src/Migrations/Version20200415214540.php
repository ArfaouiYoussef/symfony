<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200415214540 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE claim ADD content VARCHAR(255) NOT NULL, DROP start_date');
        $this->addSql('ALTER TABLE releases CHANGE project_id project_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE report CHANGE releases_id releases_id INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE claim ADD start_date DATETIME NOT NULL, DROP content');
        $this->addSql('ALTER TABLE releases CHANGE project_id project_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE report CHANGE releases_id releases_id INT DEFAULT NULL');
    }
}

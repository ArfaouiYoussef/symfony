<?php 

namespace App\Service;

use App\Entity\Project;
use Doctrine\ORM\EntityManager;

class ProjectService extends AbstractService
{

    public function __construct(EntityManager $em, $entityName)
    {
        $this->em = $em;
        $this->model = $em->getRepository($entityName);
    }

    public function getModel()
    {
        return $this->model;
    }

    public function getProjectId($id)
    {
        return $this->findOneBy(['id' => $id]);
    }

    public function getAllproject()
    {
        return $this->findAll();
    }

    public function addproject()
    {
        return $this->save();
    }

    public function deleteProject($id)
    {   
        return $this->delete($this->findOneBy(['id' => $id]));
    }




}
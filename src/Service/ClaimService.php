<?php

namespace App\Service;

use Doctrine\ORM\EntityManager;

class ClaimService extends AbstractService
{

    public function __construct(EntityManager $em, $entityName)
    {
        $this->em = $em;
        $this->model = $em->getRepository($entityName);
    }

    public function getModel()
    {
        return $this->model;
    }

    public function getClaimByid($id)
    {
        return $this->findBy(['releases' => $id]);
    }


    public function addClaim()
    {
        return $this->save();
    }
public function getAll(){
        return $this->findAll();
}
    public function deleteClaim($id)
    {
        return $this->delete($this->find($id));
    }




}
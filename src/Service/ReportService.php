<?php

namespace App\Service;

use Doctrine\ORM\EntityManager;

class ReportService extends AbstractService
{

    public function __construct(EntityManager $em, $entityName)
    {
        $this->em = $em;
        $this->model = $em->getRepository($entityName);
    }

    public function getModel()
    {
        return $this->model;
    }

    public function getById($id)
    {
        return $this->findOneBy(["id"=>$id]);
    }
    public function getReportById($id)
    {
        return $this->findAll();
    }
    public function deletereport($id)
    {
        return $this->delete($this->findOneBy(['id' => $id]));
    }






}
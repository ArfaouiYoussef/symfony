<?php

namespace App\Service;

use Doctrine\ORM\EntityManager;

class RelaseService extends AbstractService
{

    public function __construct(EntityManager $em, $entityName)
    {
        $this->em = $em;
        $this->model = $em->getRepository($entityName);
    }

    public function getModel()
    {
        return $this->model;
    }

    public function getById($id)
    {
        return $this->findOneBy(['id' => $id]);
    }

    public function getReleaseById($id)
    {
        return $this->findBy(['project' => $id]);
    }


    public function getAll()

    {
        return $this->findAll();
    }






}
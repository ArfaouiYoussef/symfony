<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use function Symfony\Component\String\u;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="claim")
 */
class Claim
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @var Releases
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Releases",  cascade={"persist"},inversedBy="claim")
     */
    private $releases;


    public function setReleases(?Releases $releases): self

    {
        $this->releases = $releases;
        return $this;
    }



    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $content;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }


    public function getReleases()
    {
        return $this->releases;
    }

    /**
     * @return string
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }


}
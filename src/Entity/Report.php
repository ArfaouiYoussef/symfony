<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use function Symfony\Component\String\u;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="report")
 *
 */
class Report
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="comment.blank")
     * @Assert\Length(
     *     min=5,
     *     minMessage="comment.too_short",
     *     max=10000,
     *     maxMessage="comment.too_long"
     * )
     */
    private $chart;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return Releases
     */
    public function getReleases(): Releases
    {
        return $this->releases;
    }
    /**
     * @var Releases
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Releases",  cascade={"persist"},inversedBy="report")
     */
    private $releases;


    public function setReleases(?Releases $releases): self

    {
        $this->releases = $releases;
        return $this;
    }

    /**
     * @return string
     */
    public function getChart(): ?string
    {
        return $this->chart;
    }

    /**
     * @param string $chart
     */
    public function setChart(string $chart): void
    {
        $this->chart = $chart;
    }






}

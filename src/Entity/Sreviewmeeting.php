<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sreviewmeeting
 *
 * @ORM\Table(name="sreviewmeeting")
 * @ORM\Entity
 */
class Sreviewmeeting
{
    /**
     * @var int
     *
     * @ORM\Column(name="idReviewMeeting", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idreviewmeeting;

    /**
     * @var string
     *
     * @ORM\Column(name="nameReview", type="string", length=254, nullable=false)
     */
    private $namereview;

    /**
     * @var string
     *
     * @ORM\Column(name="goal", type="string", length=254, nullable=false)
     */
    private $goal;

    /**
     * @var string
     *
     * @ORM\Column(name="issues", type="string", length=254, nullable=false)
     */
    private $issues;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateMeeting", type="date", nullable=false)
     */
    private $datemeeting;

    /**
     * @var string
     *
     * @ORM\Column(name="duration", type="string", length=10, nullable=false)
     */
    private $duration;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=254, nullable=false)
     */
    private $location;

    public function getIdreviewmeeting(): ?int
    {
        return $this->idreviewmeeting;
    }

    public function getNamereview(): ?string
    {
        return $this->namereview;
    }

    public function setNamereview(string $namereview): self
    {
        $this->namereview = $namereview;

        return $this;
    }

    public function getGoal(): ?string
    {
        return $this->goal;
    }

    public function setGoal(string $goal): self
    {
        $this->goal = $goal;

        return $this;
    }

    public function getIssues(): ?string
    {
        return $this->issues;
    }

    public function setIssues(string $issues): self
    {
        $this->issues = $issues;

        return $this;
    }

    public function getDatemeeting(): ?\DateTimeInterface
    {
        return $this->datemeeting;
    }

    public function setDatemeeting(\DateTimeInterface $datemeeting): self
    {
        $this->datemeeting = $datemeeting;

        return $this;
    }

    public function getDuration(): ?string
    {
        return $this->duration;
    }

    public function setDuration(string $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(string $location): self
    {
        $this->location = $location;

        return $this;
    }


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Meeting
 *
 * @ORM\Table(name="meeting")
 * @ORM\Entity
 */
class Meeting
{
    /**
     * @var int
     *
     * @ORM\Column(name="idMeeting", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmeeting;

    /**
     * @var string
     *
     * @ORM\Column(name="typeMeeting", type="string", length=254, nullable=false)
     */
    private $typemeeting;

    /**
     * @var int
     *
     * @ORM\Column(name="idProject", type="integer", nullable=false)
     */
    private $idproject;

    public function getIdmeeting(): ?int
    {
        return $this->idmeeting;
    }

    public function getTypemeeting(): ?string
    {
        return $this->typemeeting;
    }

    public function setTypemeeting(string $typemeeting): self
    {
        $this->typemeeting = $typemeeting;

        return $this;
    }

    public function getIdproject(): ?int
    {
        return $this->idproject;
    }

    public function setIdproject(int $idproject): self
    {
        $this->idproject = $idproject;

        return $this;
    }


}

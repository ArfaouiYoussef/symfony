<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Message
 *
 * @ORM\Table(name="message", indexes={@ORM\Index(name="idR", columns={"idR"}), @ORM\Index(name="id", columns={"id"})})
 * @ORM\Entity
 */
class Message
{
    /**
     * @var int
     *
     * @ORM\Column(name="idM", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idm;

    /**
     * @var string
     *
     * @ORM\Column(name="msg", type="string", length=100, nullable=false)
     */
    private $msg;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $id = 'NULL';

    /**
     * @var int|null
     *
     * @ORM\Column(name="idR", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $idr = 'NULL';

    public function getIdm(): ?int
    {
        return $this->idm;
    }

    public function getMsg(): ?string
    {
        return $this->msg;
    }

    public function setMsg(string $msg): self
    {
        $this->msg = $msg;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getIdr(): ?int
    {
        return $this->idr;
    }

    public function setIdr(?int $idr): self
    {
        $this->idr = $idr;

        return $this;
    }


}

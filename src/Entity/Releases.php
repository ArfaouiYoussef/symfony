<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use function Symfony\Component\String\u;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="releases")
 */
class Releases
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Project",  cascade={"persist"},inversedBy="releases")
     */
    private $project;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="comment.blank")

     */
    private $progress;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $startDate;
    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $releaseDate;
    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $version;
    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $status;
    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $description;





    public function getId(): ?int
    {
        return $this->id;
    }
    /**
     * @var Claim[]|ArrayCollection
     *
     * @ORM\OneToMany(
     *      targetEntity="Claim",
     *      mappedBy="Relases",
     * )
     */
    private $claim;
    /**
     * @var Report[]|ArrayCollection
     *
     * @ORM\OneToMany(
     *      targetEntity="Report",
     *      mappedBy="Relases",
     * )
     */
    private $report;

    /**
     * @return Project
     */
    public function getProject(): Project
    {
        return $this->project;
    }


    public function setProject(?Project $project): self
    {
        $this->project = $project;
        return $this;
    }

    /**
     * @return string
     */
    public function getStartDate(): ?string
    {
        return $this->startDate;
    }

    /**
     * @param string $startDate
     */
    public function setStartDate(string $startDate): void
    {
        $this->startDate = $startDate;
    }

    /**
     * @return string
     */
    public function getVersion(): ?string
    {
        return $this->version;
    }

    /**
     * @param string $version
     */
    public function setVersion(string $version): void
    {
        $this->version = $version;
    }

    /**
     * @return string
     */
    public function getProgress(): ?string
    {
        return $this->progress;
    }

    /**
     * @param string $progress
     */
    public function setProgress(string $progress): void
    {
        $this->progress = $progress;
    }

    /**
     * @return string
     */
    public function getReleaseDate():? string
    {
        return $this->releaseDate;
    }

    /**
     * @param string $releaseDate
     */
    public function setReleaseDate(string $releaseDate): void
    {
        $this->releaseDate = $releaseDate;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return Claim[]|ArrayCollection
     */
    public function getClaim()
    {
        return $this->claim;
    }

    /**
     * @param Claim[]|ArrayCollection $claim
     */
    public function setClaim($claim): void
    {
        $this->claim = $claim;
    }

    /**
     * @return string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return Report[]|ArrayCollection
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * @param Report[]|ArrayCollection $report
     */
    public function setReport($report): void
    {
        $this->report = $report;
    }




}

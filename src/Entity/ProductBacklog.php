<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductBacklog
 *
 * @ORM\Table(name="product_backlog")
 * @ORM\Entity
 */
class ProductBacklog
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_PBacklog", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPbacklog;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=254, nullable=false)
     */
    private $name;

    public function getIdPbacklog(): ?int
    {
        return $this->idPbacklog;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }


}

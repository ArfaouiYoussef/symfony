<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Retrospectivemeeting
 *
 * @ORM\Table(name="retrospectivemeeting")
 * @ORM\Entity
 */
class Retrospectivemeeting
{
    /**
     * @var int
     *
     * @ORM\Column(name="idRetroMeeting", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idretromeeting;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=254, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="goal", type="string", length=50, nullable=false)
     */
    private $goal;

    /**
     * @var string
     *
     * @ORM\Column(name="issues", type="string", length=254, nullable=false)
     */
    private $issues;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="duration", type="string", length=10, nullable=false)
     */
    private $duration;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=254, nullable=false)
     */
    private $location;

    public function getIdretromeeting(): ?int
    {
        return $this->idretromeeting;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getGoal(): ?string
    {
        return $this->goal;
    }

    public function setGoal(string $goal): self
    {
        $this->goal = $goal;

        return $this;
    }

    public function getIssues(): ?string
    {
        return $this->issues;
    }

    public function setIssues(string $issues): self
    {
        $this->issues = $issues;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getDuration(): ?string
    {
        return $this->duration;
    }

    public function setDuration(string $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(string $location): self
    {
        $this->location = $location;

        return $this;
    }


}
